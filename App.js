import React from 'react';
import {Modal, TouchableHighlight, Alert, ScrollView, Image} from 'react-native';
import { StyleSheet, Text, View, TouchableOpacity } from 'react-native';
import {AsyncStorage, AppRegistry, TextInput, Button} from 'react-native';
import { createStackNavigator, createAppContainer } from "react-navigation";

import Api from './services/Api.js'


class Connexion extends React.Component {
  constructor(props, context){
    super(props, context)
    this.api = new Api()
    this.state = {
      User: {},
      Token: null,
      email: "",
      password: "",
      showBox:{
        show: false,
        success: false,
        message: ""
      }
    }

    this.submitConnexion = this.submitConnexion.bind(this)

  }


  _storeData = async (key, value) => {
    try {
      console.log(value)
      await AsyncStorage.setItem(key, JSON.stringify(value));
    } catch (error) {
      console.log(error)
    }
  }

  _retrieveData = async (key) => {
    try {
      const value = await AsyncStorage.getItem(key);

      if (value !== null) {
        // We have data!!
        return value;
      }
    } catch (error) {
      return null
    }
  }



  componentDidMount() { // fonction qui est executée autamiquement quand tout le DOM est chargé.
    // vérfions la connexion
    (async ()=>{

      // on tente de récupérer le token
      let token = await this._retrieveData("token")

      if(token){
        // ça va
        token = JSON.parse(token)
        this.api.checkTresorier(token).then(res=>{
          let data = res.data;

          if(data.success){
            console.log("User connected")
            this.setState({User: data.tresorier})
            this.setState({Token: data.token})
            this._storeData("user", data.tresorier)
            this.props.navigation.navigate('Home')
          }else{
            // user pas valide
            console.log("User not connected")
            AsyncStorage.removeItem('token')
            AsyncStorage.removeItem('user')
          }
        })
      }else {
        // le user n'est pas connecté rester sur la même page

      }

    })()



  }


  submitConnexion(){

  		this.api.loginTresorier(this.state.email, this.state.password).then(res=>{
  			let box = {
          show: true,
          success: res.data.success,
          message: res.data.message
        }
        this.setState({showBox: box})
  			if(res.data.success){

  				// maintenant on stocke le token et le user dans localstorage
          this.setState({User: res.data.tresorier})
          this.setState({Token: res.data.token})
          this._storeData("user", res.data.tresorier)
          this._storeData("token", res.data.token)

          // on redirige vers home
          this.props.navigation.navigate('Home')
  			}

  		})
  	}

  render() {

      return (


              <View style={styles.container}>
                <Text style={{color: "red", fontSize: 20}}>Espace Trésorier</Text>
                <TextInput
                  style={{height: 40, width: "80%", borderBottomColor:"blue", borderBottomWidth: 2}}
                  onChangeText={(email) => this.setState({email})}
                  placeholder="Email"
                  autoCapitalize={"none"}
                  value={this.state.email}
                />
                <TextInput
                  style={{height: 40, width: "80%", borderBottomColor:"blue", borderBottomWidth: 2}}
                  onChangeText={(password) => this.setState({password})}
                  placeholder="Password"
                  value={this.state.password}
                  secureTextEntry={true}
                />
                <Button
                  onPress={this.submitConnexion}
                  title="Se connecter"
                  color="#841584"
                />


                {this.state.showBox.show ?
                <View>
                <Text> {this.state.showBox.message}</Text>
                </View>
                : null}

           </View>

      )


  }

}

class Home extends React.Component{
  constructor(props, context){
    super(props, context)
    this.api = new Api()
    this.state = {
      modalVisible:false,
      User: {},
      Token:null,
      numLicence: 0, // le numero de licence que le trésorier rentre dans la barre de recherche
      searchUser: {
        bordereaux: []
      }, // variable dans laquelle on stocke ce que le serveur a renvoyé après la recherche
    }

    this.getUser = this.getUser.bind(this);
    this.setModalVisible = this.setModalVisible.bind(this);
    this.validateBordereau = this.validateBordereau.bind(this);
  }

  setModalVisible(visible) {
      this.setState({modalVisible: visible});
    }

  getUser(){
    this.api.getUser(this.state.numLicence, this.state.Token).then(res=>{
      console.log(res.data)
      if(res.data.success){
				let user = res.data.user;
				user["bordereaux"] = res.data.bordereaux;
				this.setState({searchUser: res.data.user})

			}else{

			}
    })
  }

    validateBordereau(bordereau){
      console.log("hello");
      let token = this.state.Token;
     console.log(this.state.Token);
       if(token){
        this.api.validerBordereau(bordereau, token).then(res =>{
          this.setModalVisible(!this.state.modalVisible);
          console.log(res.success);
        })
      }
  }

  _storeData = async (key, value) => {
    try {
      console.log(value)
      await AsyncStorage.setItem(key, JSON.stringify(value));
    } catch (error) {
      console.log(error)
    }
  }

  _retrieveData = async (key) => {
    try {
      const value = await AsyncStorage.getItem(key);

      if (value !== null) {
        // We have data!!
        return value;
      }
    } catch (error) {
      return null
    }
  }


  componentWillMount(){

    (async ()=>{

      // on tente de récupérer le token
      let token = await this._retrieveData("token");
      let user = await this._retrieveData("user");

      this.setState({User: JSON.parse(user)})
      this.setState({Token: JSON.parse(token)})

    })()
  }

  render(){


    return (
      <View>

        <Text style={{color: "red", fontSize: 20, marginTop: 10}}>Chercher un utilisateur</Text>
        <TextInput
          style={{height: 40, width: "80%"}}
          onChangeText={(numLicence) => this.setState({numLicence})}
          placeholder="Numéro licence"
          autoCapitalize={"none"}
          value={this.state.numLicence}
        />

        <Button
          onPress={this.getUser}
          title="Rechercher"
          color="#841584"
        />
        {this.state.searchUser.bordereaux.map(bordereau => {
          return (
            <View>
              <TouchableOpacity
                style={styles.button}
                key={bordereau._id}
                onPress={() => {
                  this.setModalVisible(true);
                }}
                //onPress={()=>}
              >
                <Text>Bordereau {bordereau.annee}</Text>
                <Text> {bordereau.validated} </Text>

              </TouchableOpacity>

              <Modal
                style={styles.container}
                animationType="slide"
                transparent={false}
                visible={this.state.modalVisible}
                onRequestClose={() => {
                  Alert.alert('Modal has been closed.');
                }}>
                <ScrollView style={{marginTop: 22}}>
                  <View>
                    <Text style={styles.frais}>LISTE DES FRAIS DU BORDEREAU</Text>
                    <Text style={styles.frais}> Bordereau {bordereau.annee} </Text>
                    <Text style={styles.frais}> Bordereau ID :{bordereau._id} </Text>
                    { bordereau.validated === true ?
                      <View>
                      <Text style={{color:"green", fontWeight:"bold", fontSize:20}}>le bordereau est validé</Text>
                      <Image
                        style={{width: 50, height: 50}}
                        source={{uri: 'http://www.certim.fr/wp-content/uploads/2017/06/GreenCheckButton.png'}}
                      />
                      </View>
                      :
                      <View>
                      <Text style={{color:"red", fontWeight:"bold", fontSize:20}}> le bordereau n'a pas encore été validé </Text>
                      <TouchableOpacity
                      onPress={() => {
                        this.validateBordereau(bordereau)
                      }}
                      style={styles.button}
                      ><Text> valider Bordereau</Text></TouchableOpacity>
                      </View>
                     }

                    {bordereau.frais.map((frais, index) =>{
                    return (
                      <View>
                      <View style={{height: 50, width: '100%', backgroundColor: '#C8C8C8'}}/>
                        <Text style={styles.fraisTitle}> frais n° {index} </Text>
                        <Text style={styles.frais}> déplacement : {frais.distance} km</Text>
                        <Text style={styles.frais}> date : { frais.date} </Text>
                        <Text style={styles.frais}> description : {frais.motif} </Text>
                        <Text style={styles.frais}> cout hébergement : {frais.hebergement} €</Text>
                        <Text style={styles.frais}> cout trajet : {frais.coutTrajet} €</Text>
                        <Text style={styles.frais}> cout repas : {frais.repas} €</Text>
                        <Text style={styles.frais}> cout peages : {frais.peages} €</Text>
                        <Text style={styles.frais}> cout TTC : {frais.total} € </Text>
                      </View>)
                    })}

                    <TouchableHighlight>
                    <Button
                      onPress={() => {
                        this.setModalVisible(!this.state.modalVisible);
                      }}
                        title="Masquer Bordereau"
                        color="#841584"
                        accessibilityLabel="Learn more about this purple button"
                      />


                    </TouchableHighlight>
                  </View>
                </ScrollView>
              </Modal>

            </View>
          )
        })}

      </View>
    )
  }
}

class Bordereau extends React.Component{
  constructor(props, context){
    super(props, context)
    this.state = {
      bordereau:{},
    }
  }

}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  button: {
    alignItems: 'center',
    backgroundColor: '#DDDDDD',
    padding: 10
  },
  fraisTitle:{
    fontWeight:'bold',
    fontSize: 25,
    color: 'blue',
    alignItems: 'center',
    justifyContent: 'center',
    height: 40,
    width: "80%",
    borderBottomColor:"blue",
    borderBottomWidth: 2},
  frais: {
    fontWeight:'bold',
    fontSize: 15,
    color: 'blue',
    alignItems: 'center',
    justifyContent: 'center',
    height: 40,
    width: "80%",
    borderBottomColor:"blue",
    borderBottomWidth: 2
  }
});


const AppNavigator = createStackNavigator(
  {
    Connexion: Connexion,
    Home: Home,
    Bordereau: Bordereau
  },
  {
    initialRouteName: "Connexion"
  }

);

export default createAppContainer(AppNavigator);
