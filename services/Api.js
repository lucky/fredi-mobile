import axios from 'axios';
import {AsyncStorage} from 'react-native';


class Api {
	constructor(){
		this.url = "https://server-fredi.herokuapp.com"
	//	this.url = "http://localhost:3100"
	}


	loginTresorier(email, password){
		return axios.post(this.url+"/tresoriers/login", {
			email: email,
			password: password
		})
	}

	getUser(numLicence, token){
		return axios.post(this.url+"/tresoriers/getUser", {
			token: token,
			numLicence: numLicence
		})
	}

	 validerBordereau(Bordereau, token){
		 console.log('validerbordereau');
		return axios.post(this.url+"/tresoriers/validerbordereau", {
			token:  token,
			bordereau: Bordereau
		})
	}

// new fonction
	getBordereaux(){
		return axios.post(this.url+"/users/getBordereaux", {
			token: window.localStorage.getItem("token")
		})
	}

	addBordereau(Bordereau){
		return axios.put(this.url+"/users/addBordereau", {
			bordereau: Bordereau,
			token: window.localStorage.getItem("token")

	})
}


	deleteBordereau(bordereau){
		return axios.post(this.url+"/users/deleteBordereau", {
			bordereau : bordereau,
			token: window.localStorage.getItem("token")

		})
	}

	exportBordereau(id){
		return axios({
		  url: this.url+"/users/exportBordereau/"+id+"/"+window.localStorage.getItem("token"),
		  method: 'GET',
		  responseType: 'blob', // important
		})

	}

	register( email, password, numLicence, numMobile, nom, prenom, dateNaissance){
		return axios.put(this.url+"/users/register",{
			email: email,
			password: password,
			numLicence: numLicence,
			numMobile: numMobile,
			nom:nom,
			prenom:prenom,
			dateNaissance:dateNaissance
		})
	}

	supprimerUser(){
		return axios.post(this.url+"/users/supprimerUser", {
			token: window.localStorage.getItem("token")
		})
	}



	checkTresorier(token){
		return axios.post(this.url+"/tresoriers/checkTresorier", {
			token: token
		})
	}

}


export default Api
